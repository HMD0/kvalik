package com.example.kvalifikatsia

import android.app.Activity
import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.test.espresso.Espresso.onData
import androidx.test.rule.ActivityTestRule
import org.hamcrest.Matchers.*
import org.junit.Rule
import org.junit.Assert.assertThat

import org.junit.Assert.*

@RunWith(AndroidJUnit4::class)
class MyInstrumentedTest {

    @Rule @JvmField var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    fun test_the_list() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.kvalifikatsia", appContext.packageName)

        onView(withId(R.id.year_source)).perform(typeText("2020"), closeSoftKeyboard())
        onView(withId(R.id.countrie_code)).perform(typeText("RU"), closeSoftKeyboard())
        onView(withId(R.id.online_button)).perform(click())


    }
}

class item_Adapter(context: Context, resource:Int, array:List<Item>):
    ArrayAdapter<Item>(context, resource, array)
{
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val item = getItem(position)!!

        val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.item, null)

        val name = view.findViewById<TextView>(R.id.name)
        name.text = item.name

        val localname = view.findViewById<TextView>(R.id.localname)
        localname.text = item.localName

        val date = view.findViewById<TextView>(R.id.date)
        date.text = item.date

        val type = view.findViewById<TextView>(R.id.type)
        type.text = item.type

        return view
    }
}