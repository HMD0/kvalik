package com.example.kvalifikatsia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import retrofit2.http.GET
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Path
import java.time.Year


@Entity //отвечает за то в каком виде информация будет хпраниться на телефоне в долгосрочной памяти
data class ItemClass(
    @ColumnInfo (name="name") val ho_name:String,
    @ColumnInfo (name="locname") val ho_locname:String,
    @ColumnInfo (name="date") val ho_date:String,
    @ColumnInfo (name = "type") val ho_type:String
){
    @PrimaryKey(autoGenerate = true) var item_id: Int = 0
}

@Dao
interface ItemDao {
    @Query("Select * From ItemClass")
    fun getAll():List<ItemClass>

    @Insert
    fun insetAll (vararg items: ItemClass)
}

@Database(entities = [ItemClass::class], version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun itemDao():ItemDao
}

//==================================================================================================
//этот класс помогает расшифровывать пришедшие из интернета данные в данные пригодные для обработки приложением
class Item {
    @SerializedName("name")//должно совпадать с названием параметра на сайте
    @Expose
    var name:String? = null

    @SerializedName("localName")//должно совпадать с названием параметра на сайте
    @Expose
    var localName:String? = null

    @SerializedName("date")//должно совпадать с названием параметра на сайте
    @Expose
    var date:String? = null

    @SerializedName("type")//должно совпадать с названием параметра на сайте
    @Expose
    var type:String? = null

}

public interface RestItems{
    @GET("api/v2/PublicHolidays/{year}/{countries_code}")
    fun get_hollidays(@Path("year") year: String, @Path("countries_code") code:String):Call<List<Item>>

    @GET("rest/v2/name/{name}")
    fun getcountries(@Path("name") desc:String):Call<List<Item>>

    @GET("rest/v2/all") // сюда запрос, например positions.json, её добавят к концу URL и в итоге
    // программа должна попасть на страницу с json данными (их можно посмотреть через обычный браузер)
    fun listitems():Call<List<Item>>
}

//Преобразует входящий список классов типа Item в строку из файла item.xml
class item_Adapter(context: Context, resource:Int, array:List<Item>):
    ArrayAdapter<Item>(context, resource, array)
{
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val item = getItem(position)!!

        val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.item, null)

        val name = view.findViewById<TextView>(R.id.name)
        name.text = item.name

        val localname = view.findViewById<TextView>(R.id.localname)
        localname.text = item.localName

        val date = view.findViewById<TextView>(R.id.date)
        date.text = item.date

        val type = view.findViewById<TextView>(R.id.type)
        type.text = item.type

        return view
    }
}
//==================================================================================================

class Unit_test_support
{
    companion object tester {
        fun check_db(context: Context) : Boolean {
            val db = Room.databaseBuilder(
                context,
                AppDatabase::class.java,"items"
            ).build()
            val itemsFromDb = db.itemDao().getAll()
            return itemsFromDb.isNotEmpty()
        }

        fun internet_check() : Boolean {
            val retrofit = Retrofit.Builder().baseUrl("https://date.nager.at").//Сюда вставляем адрес сайта с которого берём данные
            addConverterFactory(GsonConverterFactory.create()).build()

            val service3 = retrofit.create(RestItems::class.java).get_hollidays("2019","RU")

            var answer = false

            try {
                val item = service3.enqueue(
                    object : Callback<List<Item>> {
                        override fun onResponse(
                            call: Call<List<Item>>,
                            response: Response<List<Item>>
                        ) {
                            if (response.body() != null) {
                                answer = true
                            }
                        }

                        override fun onFailure(call: Call<List<Item>>, t: Throwable) {
                            answer = false
                        }
                    }
                )
            }
            catch (param : Throwable)
            {
                answer = false
            }

            return answer

        }
    }

}

class Instrumented_test_support
{
    companion object tester
    {

    }
}

//==================================================================================================
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        R.id.Main_list
        val listView = findViewById<ListView>(R.id.Main_list)
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,"items"
        ).build()

        online_button.setOnClickListener {
            online_Load(db,listView,year_source.text.toString(),countrie_code.text.toString())
        }
        offline_button.setOnClickListener{
            offline_Load(db,listView)
        }
    }

    private fun offline_Load(db:AppDatabase, listView:ListView) {
        Thread(Runnable{
            val itemsFromDb = db.itemDao().getAll()
            if (itemsFromDb.isNotEmpty()) {
                listView.post(
                    Runnable {

                        listView.adapter = item_Adapter(this@MainActivity, R.layout.item,
                            itemsFromDb.map { it ->
                                Item().apply {
                                    name = it.ho_name
                                    date = it.ho_date
                                    localName = it.ho_locname
                                    type = it.ho_type
                                }
                            }
                        )

                    }
                )
            }
        }).start()
    }

    private fun online_Load(db:AppDatabase, listView:ListView, year:String, code:String){
        Thread(Runnable {

            val retrofit = Retrofit.Builder().baseUrl("https://date.nager.at").//Сюда вставляем адрес сайта с которого берём данные
            addConverterFactory(GsonConverterFactory.create()).build()

            val service = retrofit.create(RestItems::class.java).listitems()

            val service2 = retrofit.create(RestItems::class.java).getcountries("United")

            val service3 = retrofit.create(RestItems::class.java).get_hollidays(year,code)

            val items = service3.enqueue(
                object : Callback<List<Item>>{
                    override fun onResponse(
                        call: Call<List<Item>>,
                        response: Response<List<Item>>
                    ) {
                        listView.adapter = item_Adapter(this@MainActivity,
                            R.layout.item,
                            response.body()!!)

                        Thread(Runnable {

                            db.itemDao().insetAll(*response.body()!!.map {

                                ItemClass(
                                    ho_name = if (it.name != null) {it.name!!} else {"-"},
                                    ho_locname = if (it.localName != null) {it.localName!!} else {"-"},
                                    ho_date = if (it.date != null) {it.date!!} else {"-"},
                                    ho_type = if (it.type != null) {it.type!!} else {"-"}
                                )

                            }.toTypedArray())

                        }).start()
                    }

                    override fun onFailure(call: Call<List<Item>>, t: Throwable) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }
                }
            )

        }).start()
    }
}
